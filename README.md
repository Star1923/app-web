# README #

Aplicaci�n web para el curso del Mintic ciclo 03

## CONFIGURACI�N B�SICA (SOLO SE HACE LA PRIMERA VEZ)

Configurar Nombre que salen en los commits
```ssh
	git config --global user.name "Nombre"
```
Configurar Email
```ssh	
	git config --global user.email nombre@gmail.com
```

## PASOS IMPORTANTES 

Iniciamos GIT en la carpeta donde esta el proyecto
```ssh
	git init
```
Clonamos el repositorio de bitbucket
```ssh
	git clone <url>
```

## GIT ADD

A�adimos todos los archivos para el commit
```ssh
	git add .
```

## GIT COMMIT

Cargar en el HEAD los cambios realizados
```ssh
	git commit -m "Texto que identifique por que se hizo el commit"
```

Agregar y Cargar en el HEAD los cambios realizados
```ssh
	git commit -a -m "Texto que identifique por que se hizo el commit"
```

De haber conflictos los muestra
```ssh
	git commit -a 
```

Agregar al ultimo commit, este no se muestra como un nuevo commit en los logs. Se puede especificar un nuevo mensaje
```ssh
	git commit --amend -m "Texto que identifique por que se hizo el commit"
```

## GIT PUSH

Subimos al repositorio
```ssh
	git push <origien> <branch>
```

## NOTA 

Si ya lo tienes descargo en tu computador, recuerda hacer un git pull antes de trabajar 


